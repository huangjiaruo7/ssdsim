/*****************************************************************************************************************************
This project was supported by the National Basic Research 973 Program of China under Grant No.2011CB302301
Huazhong University of Science and Technology (HUST)   Wuhan National Laboratory for Optoelectronics

FileName： ssd.c
Author: Hu Yang		Version: 2.1	Date:2011/12/02
Description:

History:
<contributor>     <time>        <version>       <desc>                   <e-mail>
Yang Hu	        2009/09/25	      1.0		    Creat SSDsim       yanghu@foxmail.com
                2010/05/01        2.x           Change 
Zhiming Zhu     2011/07/01        2.0           Change               812839842@qq.com
Shuangwu Zhang  2011/11/01        2.1           Change               820876427@qq.com
Chao Ren        2011/07/01        2.0           Change               529517386@qq.com
Hao Luo         2011/01/01        2.0           Change               luohao135680@gmail.com
*****************************************************************************************************************************/

 

#include "ssd.h"

/********************************************************************************************************************************
1，main函数中initiatio()函数用来初始化ssd；
2，make_aged()函数使SSD成为aged，aged的ssd相当于使用过一段时间的ssd，里面有失效页，
	non_aged的ssd是新的ssd，无失效页，失效页的比例可以在初始化参数中设置；
3，pre_process_page()函数提前扫一遍读请求，把读请求的lpn<--->ppn映射关系事先建立好，
	写请求的lpn<--->ppn映射关系在写的时候再建立，预处理trace防止读请求是读不到数据；
4，simulate()是核心处理函数，trace文件从读进来到处理完成都由这个函数来完成；
5，statistic_output()函数将ssd结构中的信息输出到输出文件，输出的是
	统计数据和平均数据，输出文件较小，trace_output文件则很大很详细；
6，free_all_node()函数释放整个main函数中申请的节点
*********************************************************************************************************************************/
int  main()
{
	unsigned  int i,j,k;
	struct ssd_info *ssd;

	#ifdef DEBUG
	printf("enter main\n"); 
	#endif

	ssd=(struct ssd_info*)malloc(sizeof(struct ssd_info));
	alloc_assert(ssd,"ssd");
	memset(ssd,0, sizeof(struct ssd_info));

	ssd=initiation(ssd);
	make_aged(ssd);
	pre_process_page(ssd);

	for (i=0;i<ssd->parameter->channel_number;i++)
	{
		for (j=0;j<ssd->parameter->die_chip;j++)
		{
			for (k=0;k<ssd->parameter->plane_die;k++)
			{
				printf("%d,0,%d,%d:  %5d\n",
						i,j,k,ssd->channel_head[i].chip_head[0].die_head[j].plane_head[k].free_page);
			}
		}
	}
	fprintf(ssd->outputfile,"\t\t\t\t\t\t\t\t\tOUTPUT\n");
	fprintf(ssd->outputfile,"****************** TRACE INFO ******************\n");

	ssd=simulate(ssd);
	statistic_output(ssd);

	printf("\n");
	printf("the simulation is completed!\n");
	
	return 1;
/* 	_CrtDumpMemoryLeaks(); */
}


/******************simulate() *********************************************************************
*simulate()是核心处理函数，主要实现的功能包括
*1. 从trace文件中获取一条请求，挂到ssd->request
*2. 根据ssd是否有dram分别处理读出来的请求，
	把这些请求处理成为读写子请求，挂到ssd->channel或者ssd上
*3. 按照事件的先后来处理这些读写子请求。
*4. 输出每条请求的子请求都处理完后的相关信息到outputfile文件中
**************************************************************************************************/
struct ssd_info *simulate(struct ssd_info *ssd)
{
	int flag=1,flag1=0;
	double output_step=0;
	unsigned int a=0,b=0;
	//errno_t err;

	printf("\n");
	printf("begin simulating.......................\n");
	printf("\n");
	printf("\n");
	printf("   ^o^    OK, please wait a moment, and enjoy music and coffee   ^o^    \n");

	ssd->tracefile = fopen(ssd->tracefilename,"r");
	if(ssd->tracefile == NULL)
	{  
		printf("the trace file can't open\n");
		return NULL;
	}

	fprintf(ssd->outputfile,"      arrive           lsn     size ope     begin time    response time    process time\n");	
	fflush(ssd->outputfile);

	while(flag!=100)      
	{
        
		flag=get_requests(ssd);

		if(flag == 1)
		{
			if (ssd->parameter->dram_capacity!=0)
			{
				buffer_management(ssd);
				distribute(ssd);
			}
			else
			{
				no_buffer_distribute(ssd);
			}
		}

		process(ssd);    
		trace_output(ssd);
		if(flag == 0 && ssd->request_queue == NULL)
			flag = 100;
	}

	fclose(ssd->tracefile);
	return ssd;
}

/**************************************************************************
@function get_request():
	1.取得已经到达的请求;
	2.将这些请求添加到ssd->request_queue链表中;
@return: 
	0:到trace文件末尾; 
	1：添加一条新请求到链表中;
	-1: 没有请求添加;
@执行流程: 
若trace文件未到末尾, 读入一行请求. 
调用find_nearest_event()找到最近的应该处理的事件时间, 
一般是上一条请求的子请求经历命令传输/数据传输, 读写操作等阶段时会预测当前阶段完成之后可以执行下一步的时间. 
比较这个predict_time与新请求的到达时间, 决定process先处理哪一个(继续处理子请求还是先响应新请求).
********************************************************************************/
int get_requests(struct ssd_info *ssd)  
{
	#ifdef DEBUG
		printf("enter get_requests, current time:%lld\n", ssd->current_time);
	#endif

	char buffer[200];
	unsigned int lsn = 0;
	int device, size, ope, large_lsn;
	int i = 0,j = 0, flag = 1;
	struct request *request1;
	long filepoint;
	int64_t time_t = 0, nearest_event_time;
	struct read_sample_node *curr_read_sample_node, *tmp_sample_node;

	if(feof(ssd->tracefile))
		return 100;

	filepoint = ftell(ssd->tracefile);	
	fgets(buffer, 200, ssd->tracefile); 
	sscanf(buffer, "%lld %d %d %d %d", &time_t, &device, &lsn, &size, &ope);
	//有时候由其他格式的trace文件转化过来数据会溢出, 这个程序无法处理这种情况, 
	//还是预先修改或删掉吧.
	if (time_t < 0 || device < 0 || lsn <= 0 || size <= 0 || ope < 0) 
	{
		//根据请求到达时间与其lsn就可以定位
		printf("It seems that there are some invalid arguments in the request\n");
		printf("Maybe you could modify or delete it\n");
		printf("Current request time is %lld, and it\'s lsn is %d\n", time_t, lsn);
		printf("if the time and the lsn are both 0, maybe you need delete the empty line at the last one\n");
		return 100;
	}
	//这两个if没什么用, 只是个记录而已, 不用管它
	if (lsn < ssd->min_lsn) ssd->min_lsn = lsn;
	if (lsn > ssd->max_lsn) ssd->max_lsn = lsn;

	/***********************************************************************************************
	上层文件系统发送给SSD的任何读写命令包括两个部分（lsn，size） lsn是逻辑扇区号，
	对于文件系统而言，它所看到的存储空间是一个线性的连续空间。
	例如，读请求(260, 6)表示的是需要读取从扇区号为260的逻辑扇区开始，总共6个扇区。
	
	large_lsn: channel下面有多少个subpage，即多少个sector。
	overprovide：SSD中并不是所有的空间都可以给用户使用，比如32G的SSD可能有10%的空间留作他用
	************************************************************************************************/
	large_lsn = (int)((ssd->parameter->subpage_page *
					   ssd->parameter->page_block *
					   ssd->parameter->block_plane *
					   ssd->parameter->plane_die *
					   ssd->parameter->die_chip *
					   ssd->parameter->chip_num) *
					  (1 - ssd->parameter->overprovide));
	lsn = lsn % large_lsn;

	nearest_event_time = find_nearest_event(ssd);
	//这个结果代表ssd里没有子请求
	if (nearest_event_time == MAX_INT64)
	{
		ssd->current_time = time_t;
	}
	else
	{
		//当前正在处理的请求的下一个状态的到来时间比下一条新请求到来的时间要早, 即新请求其实还未到达, 
		//所以将系统时间更新为nearest_event_time即可; 
		//如果nearest_event_time > time_t 说明在当前的请求在能进行到下一状态之前新请求已经到了, 
		//这就需要考虑将新请求挂在ssd的请求队列上(当然, 是在ssd请求队列未满的情况下)
		if(nearest_event_time < time_t)
		{
			/*******************************************************************************
			*回滚，新请求其实还未到达, 读入它也没有意义
			**********************************************************************************/
			fseek(ssd->tracefile, filepoint, 0);
			if(ssd->current_time <= nearest_event_time)
				ssd->current_time = nearest_event_time;
			return -1;
		}
		else
		{
			//挂在ssd上的请求队列太长, 就先放弃这条请求, 文件指针回滚
			if (ssd->request_queue_length >= ssd->parameter->queue_length)
			{
				fseek(ssd->tracefile, filepoint, 0);
				ssd->current_time = nearest_event_time;
				return -1;
			}
			else
			{
				//新请求到来的要早于当前请求的当前状态完成时间, 将系统时间推进为这个新请求的到达时间
				ssd->current_time = time_t;
			}
		}
	}

	if(time_t < 0)
	{
		printf("error!\n");
		while(1){}
	}
	
	if(feof(ssd->tracefile))
	{
		request1=NULL;
		return 0;
	}

	request1 = (struct request*)malloc(sizeof(struct request));
	alloc_assert(request1,"request");
	memset(request1, 0, sizeof(struct request));

	request1->time = time_t;
	request1->lsn = lsn;
	request1->size = size;
	request1->operation = ope;	
	request1->begin_time = time_t;
	request1->response_time = 0;	
	request1->energy_consumption = 0;	
	request1->next_node = NULL;
	request1->distri_flag = 0;              //代表是否应用过distribute(), 并被分发成多个子请求
	request1->subs = NULL;
	request1->need_distr_flag = NULL;
	request1->complete_lsn_count=0;         //record the count of lsn served by buffer
	filepoint = ftell(ssd->tracefile);		

	if(ssd->request_queue == NULL)
	{
		ssd->request_queue = request1;
		ssd->request_tail = request1;
		ssd->request_queue_length ++;
	}
	else
	{
		(ssd->request_tail)->next_node = request1;	
		ssd->request_tail = request1;			
		ssd->request_queue_length ++;
	}
	//计算当前读密度
	curr_read_sample_node = (struct read_sample_node *)malloc(sizeof(struct read_sample_node));
	if(ope == 0)
		curr_read_sample_node->ope = '0';
	else
		curr_read_sample_node->ope = '1';
    curr_read_sample_node->next = NULL;
	if(ssd->read_sample_head == NULL)
	{
		ssd->read_sample_head = curr_read_sample_node;
        ssd->read_sample_tail = curr_read_sample_node;
		ssd->curr_read_sample_sum = 1;
		if(ope == 0)
			ssd->curr_read_sum = 0;
		else
			ssd->curr_read_sum = 1;
	}
	else
	{
		//如果该样本队列总数还未满预设的read_sample_sum
        if(ssd->curr_read_sample_sum < ssd->parameter->read_sample_sum)
		{
			ssd->curr_read_sample_sum ++;
		}
		else
		{
			if(ssd->read_sample_head->ope == '1')
				ssd->curr_read_sum --;
			
			tmp_sample_node = ssd->read_sample_head;
			ssd->read_sample_head = ssd->read_sample_head->next;
            ssd->read_sample_head->prev = NULL;
			free(tmp_sample_node);
		}
		ssd->read_sample_tail->next = curr_read_sample_node;
		curr_read_sample_node->prev = ssd->read_sample_tail;
		ssd->read_sample_tail = curr_read_sample_node;
		if(ope == 1)
			ssd->curr_read_sum ++;
	}
	ssd->curr_read_density = (float) ssd->curr_read_sum /(float) ssd->curr_read_sample_sum;

	//计算请求平均大小 1为读 0为写
	if (request1->operation == 1)             
		ssd->ave_read_size = (ssd->ave_read_size * ssd->read_request_count + request1->size) /
							 (ssd->read_request_count + 1);
	else
		ssd->ave_write_size = (ssd->ave_write_size * ssd->write_request_count + request1->size) /
							  (ssd->write_request_count + 1);

	filepoint = ftell(ssd->tracefile);	
	fgets(buffer, 200, ssd->tracefile);    //寻找下一条请求的到达时间
	sscanf(buffer,"%lld %d %d %d %d", &time_t, &device, &lsn, &size, &ope);
	ssd->next_request_time = time_t;
	fseek(ssd->tracefile,filepoint,0);

	return 1;
}

/***************************************************************************************
*首先buffer是个写buffer，就是为写请求服务的，
	因为读flash的时间tR为20us，写flash的时间tprog为200us，所以为写操作服务更能节省时间
*读操作:	如果命中了buffer，从buffer读，不占用channel的I/O总线，
		没有命中buffer，从flash读，占用channel的I/O总线，但是不进buffer了
*写操作:	首先request分成sub_request子请求，
		如果是动态分配，sub_request挂到ssd->sub_request上，
		因为不知道要先挂到哪个channel的sub_request上
*       如果是静态分配则sub_request挂到channel的sub_request链上,

		同时不管动态分配还是静态分配sub_request都要挂到request的sub_request链上
		因为每处理完一个request，都要在traceoutput文件中输出关于这个request的信息。

		处理完一个sub_request,就将其从channel的sub_request链或ssd的sub_request链上摘除，
		但是在traceoutput文件输出一条后再清空request的sub_request链。

*		sub_request命中buffer则在buffer里面写就行了，并且将该sub_page提到buffer链头(LRU)，
		若没有命中且buffer满，则先将buffer链尾的sub_request写入flash
		(这会产生一个sub_request写请求，挂到这个请求request的sub_request链上，
		同时视动态分配还是静态分配挂到channel或ssd的sub_request链上),
		在将要写的sub_page写入buffer链头
*****************************************************************************************/
struct ssd_info *buffer_management(struct ssd_info *ssd)
{
	#ifdef DEBUG
		printf("enter buffer_management, current time:%lld\n",ssd->current_time);
	#endif

	unsigned int j, index;
	unsigned int lsn,lpn,last_lpn,first_lpn;
	unsigned int complete_flag = 0, state,full_page;
	unsigned int flag = 0, need_distb_flag, lsn_flag;
	unsigned int mask = 0,offset1 = 0,offset2 = 0;
	struct request *new_req;
	struct buffer_group *buffer_node, key;
	int subpage_page = ssd->parameter->subpage_page;
	ssd->dram->current_time = ssd->current_time;
	full_page = ~(0xffffffff << subpage_page);
	new_req = ssd->request_tail;
	lsn = new_req->lsn;

	//这些lpn难道不应该+1么
	lpn = lsn / subpage_page;
	last_lpn = (lsn + new_req->size - 1) / subpage_page;
	first_lpn = lsn / subpage_page;

	new_req->need_distr_flag = (unsigned int *)malloc(sizeof(unsigned int) * 
															((last_lpn - first_lpn + 1) *
															  subpage_page / 32 + 1));
	alloc_assert(new_req->need_distr_flag, "new_req->need_distr_flag");
	memset(new_req->need_distr_flag, 0, sizeof(unsigned int) * 
												((last_lpn - first_lpn+1) *
												subpage_page / 32 + 1));

	if(new_req->operation == READ) 
	{
		//如果lpn还未到最后一页
		while(lpn <= last_lpn)
		{
			/********************************************************************************
			 *need_distb_flag表示是否需要执行distribution函数，
			 *1表示需要执行，buffer中没有，0表示不需要执行
             *即1表示需要分发，0表示不需要分发，对应点初始全部赋为1
			********************************************************************************/
			need_distb_flag = full_page;
			key.group = lpn;
			// buffer_node, 是一个平衡二叉树节点
			buffer_node = (struct buffer_group *)avlTreeFind(ssd->dram->buffer, 
															(TREE_NODE *)&key);		

			//while的循环条件:buffer中存在我们要使用的目标页, lsn变量未超出当前操作的扇区范围
			//不过, 页命中不一定所有子页都能命中啊
			while((buffer_node != NULL) && 
				  	(lsn < (lpn + 1) * subpage_page) && 
				  	(lsn <= (new_req->lsn + new_req->size - 1)))             			
			{
				lsn_flag = full_page;
				//mask向左移
				mask = 1 << (lsn % subpage_page);
				//说明subpage_page >= 5了吧(5是位数, 而不是实际数值)
				if(mask > 31)
				{
					printf("the subpage number is larger than 32! add some cases");
					getchar();
				}
				//关于这个else if条件匹配, 注意这个while循环的步进单位是lsn(每次lsn都加1)，
				//也就是说每次都会将lsn与buffer_node中stored成员进行与操作, 
				//查看当前扇区是否被缓存在buffer中.

				//比如一个操作的size为2, 目标为一个页的第3,4个扇区, 
				//那lsn % subpage_page = 2, 相当于偏移.
				//mask 就= 0100, 如果stored=1100,那stored & mask 就 == mask了. 
				//不过要注意的是两者对扇区的排列顺序, stored, mask应该都是从低位开始计数的.
				else if((buffer_node->stored & mask) == mask)
				{
					//flag, 当前操作涉及到lsn扇区是否命中buffer
					flag = 1;
					lsn_flag = lsn_flag & (~ mask);
				}

				if(flag == 1)
				{	//如果该buffer节点不在buffer的队首，需要将这个节点提到队首，
					//实现了LRU算法，这个是一个双向队列.
					if(ssd->dram->buffer->buffer_head != buffer_node)     
					{
						if(ssd->dram->buffer->buffer_tail == buffer_node)								
						{
							buffer_node->LRU_link_pre->LRU_link_next = NULL;					
							ssd->dram->buffer->buffer_tail = buffer_node->LRU_link_pre;							
						}
						else
						{
							buffer_node->LRU_link_pre->LRU_link_next = buffer_node->LRU_link_next;				
							buffer_node->LRU_link_next->LRU_link_pre = buffer_node->LRU_link_pre;								
						}
						//将ssd->dram->buffer->buffer_head的值设为该buffer_node
						buffer_node->LRU_link_next = ssd->dram->buffer->buffer_head;
						ssd->dram->buffer->buffer_head->LRU_link_pre = buffer_node;
						buffer_node->LRU_link_pre = NULL;
						ssd->dram->buffer->buffer_head = buffer_node;													
					}
					// buffer命中次数+1
					ssd->dram->buffer->read_hit ++;
					new_req->complete_lsn_count ++;				
				}
				// 当前操作未命中buffer
				//**未命中buffer时, 不应该把当前操作所指向的目标物理页缓存到buffer吗???
				//**好吧, 这是写buffer
				else if(flag == 0)
				{
					ssd->dram->buffer->read_miss_hit ++;
				}

				need_distb_flag = need_distb_flag & lsn_flag;

				flag = 0;		
				lsn ++;
			}

			index = (lpn - first_lpn) / (32 / subpage_page); 			
			new_req->need_distr_flag[index] = new_req->need_distr_flag[index] | 
												  (need_distb_flag << (
														((lpn - first_lpn) % 
														(32 / subpage_page)) * subpage_page));	
			lpn ++;
		}
	}
	else if(new_req->operation == WRITE)
	{
		while(lpn <= last_lpn)           	
		{
			need_distb_flag = full_page;
			mask = ~(0xffffffff << (subpage_page));
			state = mask;

			if(lpn == first_lpn)
			{
				offset1 = subpage_page - ((lpn + 1) * subpage_page - new_req->lsn);
				state = state & (0xffffffff << offset1);
			}
			if(lpn == last_lpn)
			{
				offset2 = subpage_page - 
				    	  ((lpn + 1) * subpage_page - (new_req->lsn + new_req->size));
				state = state & (~ (0xffffffff << offset2));
			}
			//为主请求创建子请求, 所以子请求的值可以为NULL
			//有时候可以直接创建子请求而不需要主请求, 比如buffer已满时的写回操作
			ssd = insert2buffer(ssd, lpn, state, NULL, new_req, BUFFER_WRITE);
			lpn ++;
		}
	}

	complete_flag = 1;
	for(j = 0; j <= (last_lpn - first_lpn + 1) * subpage_page / 32; j ++)
	{
		if(new_req->need_distr_flag[j] != 0)
		{
			complete_flag = 0;
		}
	}

	/*************************************************************
	*如果请求已经被全部由buffer服务，该请求可以被直接响应，输出结果
	*这里假设dram的服务时间为1000ns
	**************************************************************/
	if((complete_flag == 1) && (new_req->subs == NULL))
	{
		new_req->begin_time = ssd->current_time;
		new_req->response_time = ssd->current_time + 1000;            
	}

	return ssd;
}

/*****************************
*lpn向ppn的转换
******************************/
unsigned int lpn2ppn(struct ssd_info *ssd,unsigned int lsn)
{
	int lpn, ppn;	
	struct entry *p_map = ssd->dram->map->map_entry;
	#ifdef DEBUG
		printf("enter lpn2ppn,  current time:%lld\n",ssd->current_time);
	#endif

	lpn = lsn/ssd->parameter->subpage_page;
	ppn = (p_map[lpn]).pn;
	return ppn;
}

/**********************************************************************************
*读请求分配子请求函数，这里只处理读请求，写请求已经在buffer_management()函数中处理了
*根据请求队列和buffer命中的检查，将每个请求分解成子请求，将子请求队列挂在channel上，
*不同的channel有自己的子请求队列
**********************************************************************************/
struct ssd_info *distribute(struct ssd_info *ssd) 
{
	unsigned int start, end;
	unsigned int first_lsn, last_lsn;
	unsigned int lpn, full_page;
	unsigned int j, k, sub_size;
	int i = 0;
	struct request *req;
	struct sub_request *sub;
	int *need_distr_flag;

	#ifdef DEBUG
		printf("enter distribute,  current time: %lld\n",ssd->current_time);
	#endif

	full_page = ~ (0xffffffff << ssd->parameter->subpage_page);
	req = ssd->request_tail;
	if(req->response_time != 0) return ssd;
	if (req->operation == WRITE) return ssd;

	if(req != NULL)
	{
		//还未经过distribute()处理
		if(req->distri_flag == 0)
		{
			//如果还有一些读子请求需要处理
			if(req->complete_lsn_count != ssd->request_tail->size)
			{
				first_lsn 		= req->lsn;
				last_lsn 		= first_lsn + req->size;
				need_distr_flag = req->need_distr_flag;
				start = first_lsn - first_lsn % ssd->parameter->subpage_page;
				end = (last_lsn / ssd->parameter->subpage_page + 1) * ssd->parameter->subpage_page;
				i = (end - start) / 32;

				while(i >= 0)
				{
					/*************************************************************************************
					*一个32位的整型数据的每一位代表一个子页，32/ssd->parameter->subpage_page就表示有多少页，
					*这里的每一页的状态都存放在了 req->need_distr_flag中，也就是need_distr_flag中，通过比较need_distr_flag的
					*每一项与full_page，就可以知道，这一页是否处理完成。如果没处理完成则通过creat_sub_request
					函数创建子请求。
					*************************************************************************************/
					for(j = 0; j < 32 / ssd->parameter->subpage_page; j ++)
					{	
						k = (need_distr_flag[((end-start)/32-i)] >>(ssd->parameter->subpage_page*j)) & full_page;	
						if (k !=0)
						{
							lpn = start / ssd->parameter->subpage_page + 
								  ((end - start) / 32 - i) * 32 / ssd->parameter->subpage_page + j;
							sub_size = transfer_size(ssd, k, lpn, req);    
							if (sub_size == 0)
								continue;
							else
								sub = creat_sub_request(ssd, lpn, sub_size, 0, req, req->operation);
						}
					}
					i = i - 1;
				}
			}
			else
			{
				req->begin_time = ssd->current_time;
				req->response_time = ssd->current_time + 1000;   
			}
		}
	}
	return ssd;
}

/**********************************************************************
*trace_output()函数是在每一条请求的所有子请求经过process()函数处理完后，
*打印输出相关的运行结果到outputfile文件中，这里的结果主要是运行的时间
**********************************************************************/
void trace_output(struct ssd_info* ssd)
{
	int flag = 1;	
	int64_t start_time, end_time;
	struct request *req, *pre_node;
	struct sub_request *sub, *tmp;

	#ifdef DEBUG
		printf("enter trace_output,  current time:%lld\n",ssd->current_time);
	#endif

	pre_node = NULL;
	req = ssd->request_queue;
	start_time = 0;
	end_time = 0;

	if(req == NULL)
		return;
	
	while(req != NULL)	
	{
		sub = req->subs;
		flag = 1;
		start_time = 0;
		end_time = 0;
	
		if(req->response_time != 0)
		{
			fprintf(ssd->outputfile, "%16lld %10d %6d %2d %16lld %16lld %10lld\n",
									 req->time, req->lsn, req->size, req->operation, req->begin_time, 
									 req->response_time, req->response_time - req->time);
			fflush(ssd->outputfile);

			if(req->response_time - req->begin_time == 0)
			{
				printf("the response time is 0?? \n");
				getchar();
			}

			if (req->operation == READ)
			{
				ssd->read_request_count ++;
				ssd->read_avg = ssd->read_avg + (req->response_time - req->time);
			}
			else
			{
				ssd->write_request_count ++;
				ssd->write_avg = ssd->write_avg + (req->response_time - req->time);
			}
			if(pre_node == NULL)
			{
				//最后一个req
				if(req->next_node == NULL)
				{
					free(req->need_distr_flag);
					req->need_distr_flag = NULL;
					free(req);
					req = NULL;
					ssd->request_queue = NULL;
					ssd->request_tail = NULL;
					ssd->request_queue_length --;
				}
				else
				{
					ssd->request_queue = req->next_node;
					pre_node = req;
					req = req->next_node;
					free(pre_node->need_distr_flag);
					pre_node->need_distr_flag = NULL;
					free((void *)pre_node);
					pre_node = NULL;
					ssd->request_queue_length --;
				}
			}
			else
			{
				if(req->next_node == NULL)
				{
					pre_node->next_node = NULL;
					free(req->need_distr_flag);
					req->need_distr_flag=NULL;
					free(req);
					req = NULL;
					ssd->request_tail = pre_node;
					ssd->request_queue_length--;
				}
				else
				{
					pre_node->next_node = req->next_node;
					free(req->need_distr_flag);
					req->need_distr_flag=NULL;
					free((void *)req);
					req = pre_node->next_node;
					ssd->request_queue_length--;
				}
			}
		}
		else
		{
			//标志位flag是确认所有子请求是否都已经执行完毕, 
			//只要有一个子请求未完成, flag就等于0并结束while循环
			flag = 1;
			while(sub != NULL)
			{
				//得到每个子请求的开始时间与结束时间, 并确认所有子请求都已经执行完毕, 
				//这时, 最小的开始时间是它们父请求的开始时间, 而最大的结束时间是父请求的结束时间.
				if(start_time == 0)
					start_time = sub->begin_time;
				if(start_time > sub->begin_time)
					start_time = sub->begin_time;
				if(end_time < sub->complete_time)
					end_time = sub->complete_time;
				//只有所有子请求的下一状态为SR_COMPLETE时才输出主请求的处理信息, 
				//只要有一个子请求未完成, flag就被赋值为0, 转而处理ssd中下一个主请求
				if((sub->current_state == SR_COMPLETE) ||
					((sub->next_state == SR_COMPLETE) &&
					(sub->next_state_predict_time <= ssd->current_time)))	
				{
					sub = sub->next_subs;
				}
				else
				{
					flag = 0;
					break;
				}
			}
			if (flag == 1)
			{
				//当前父请求执行完毕, 打印对其的处理结果
				fprintf(ssd->outputfile,"%16lld %10d %6d %2d %16lld %16lld %10lld\n",
										req->time, req->lsn, req->size, req->operation,
										start_time, end_time, end_time-req->time);
				fflush(ssd->outputfile);

				if(end_time-start_time==0)
				{
					printf("the response time is 0?? \n");
					getchar();
				}

				if (req->operation==READ)
				{
					ssd->read_request_count++;
					ssd->read_avg=ssd->read_avg+(end_time-req->time);
				}
				else
				{
					ssd->write_request_count++;
					ssd->write_avg=ssd->write_avg+(end_time-req->time);
				}
				//依次释放当前请求下的所有子请求
				while(req->subs != NULL)
				{
					tmp = req->subs;
					req->subs = tmp->next_subs;
					if(tmp->update != NULL)
					{
						free(tmp->update->location);
						tmp->update->location = NULL;
						free(tmp->update);
						tmp->update = NULL;
					}
					free(tmp->location);
					tmp->location = NULL;
					free(tmp);
					tmp = NULL;
				}
				//根据当前请求在request队列中的位置, 释放当前请求空间
				if(pre_node == NULL)
				{
					if(req->next_node == NULL)
					{
						free(req->need_distr_flag);
						req->need_distr_flag=NULL;
						free(req);
						req = NULL;
						ssd->request_queue = NULL;
						ssd->request_tail = NULL;
						ssd->request_queue_length--;
					}
					else
					{
						ssd->request_queue = req->next_node;
						pre_node = req;
						req = req->next_node;
						free(pre_node->need_distr_flag);
						pre_node->need_distr_flag=NULL;
						free(pre_node);
						pre_node = NULL;
						ssd->request_queue_length--;
					}
				}
				else
				{
					if(req->next_node == NULL)
					{
						pre_node->next_node = NULL;
						free(req->need_distr_flag);
						req->need_distr_flag=NULL;
						free(req);
						req = NULL;
						ssd->request_tail = pre_node;	
						ssd->request_queue_length--;
					}
					else
					{
						pre_node->next_node = req->next_node;
						free(req->need_distr_flag);
						req->need_distr_flag=NULL;
						free(req);
						req = pre_node->next_node;
						ssd->request_queue_length --;
					}
				}
			}
			else
			{	
				pre_node = req;
				req = req->next_node;
			}
		}// end if(req->response_time != 0)
	}// end while(req != NULL)
}

/*******************************************************************************
*statistic_output()函数主要是输出处理完一条请求后的相关处理信息。
*1，计算出每个plane的擦除次数即plane_erase和总的擦除次数即erase
*2，打印min_lsn，max_lsn，read_count，program_count等统计信息到文件outputfile中。
*3，打印相同的信息到文件statisticfile中
*******************************************************************************/
void statistic_output(struct ssd_info *ssd)
{
	unsigned int lpn_count=0,i,j,k,m,erase=0,plane_erase=0;
	double gc_energy=0.0;
	#ifdef DEBUG
		printf("enter statistic_output,  current time:%lld\n",ssd->current_time);
	#endif

	for(i=0; i < ssd->parameter->channel_number; i++)
	{
		for(j=0;j<ssd->parameter->die_chip;j++)
		{
			for(k=0;k<ssd->parameter->plane_die;k++)
			{
				plane_erase=0;
				for(m=0;m<ssd->parameter->block_plane;m++)
				{
					if(ssd->channel_head[i].chip_head[0].die_head[j].plane_head[k].blk_head[m].erase_count>0)
					{
						erase=erase+ssd->channel_head[i].chip_head[0].die_head[j].plane_head[k].blk_head[m].erase_count;
						plane_erase+=ssd->channel_head[i].chip_head[0].die_head[j].plane_head[k].blk_head[m].erase_count;
					}
				}
				fprintf(ssd->outputfile,"the %d channel, %d chip, %d die, %d plane has : %13d erase operations\n",i,j,k,m,plane_erase);
				fprintf(ssd->statisticfile,"the %d channel, %d chip, %d die, %d plane has : %13d erase operations\n",i,j,k,m,plane_erase);
			}
		}

	}
	
	//当ssd->read_request_count或write_request_count等于0时不能作除数
	int64_t read_avg_response_time;
	int64_t write_avg_response_time;
	if(ssd->read_request_count == 0){
		read_avg_response_time = 0;
	}else{
		read_avg_response_time = ssd->read_avg/ssd->read_request_count;
	}

	if(ssd->write_request_count == 0){
		write_avg_response_time = 0;
	}else{
		write_avg_response_time = ssd->write_avg/ssd->write_request_count;
	}
	
	fprintf(ssd->outputfile,"\n");
	fprintf(ssd->outputfile,"\n");
	fprintf(ssd->outputfile,"---------------------------statistic data---------------------------\n");	 
	fprintf(ssd->outputfile,"min lsn: %13d\n",ssd->min_lsn);	
	fprintf(ssd->outputfile,"max lsn: %13d\n",ssd->max_lsn);
	fprintf(ssd->outputfile,"read count: %13d\n",ssd->read_count);	  
	fprintf(ssd->outputfile,"program count: %13d",ssd->program_count);	
	fprintf(ssd->outputfile,"                        include the flash write count leaded by read requests\n");
	fprintf(ssd->outputfile,"the read operation leaded by un-covered update count: %13d\n",ssd->update_read_count);
	fprintf(ssd->outputfile,"erase count: %13d\n",ssd->erase_count);
	fprintf(ssd->outputfile,"direct erase count: %13d\n",ssd->direct_erase_count);
	fprintf(ssd->outputfile,"copy back count: %13d\n",ssd->copy_back_count);
	fprintf(ssd->outputfile,"multi-plane program count: %13d\n",ssd->m_plane_prog_count);
	fprintf(ssd->outputfile,"multi-plane read count: %13d\n",ssd->m_plane_read_count);
	fprintf(ssd->outputfile,"interleave write count: %13d\n",ssd->interleave_count);
	fprintf(ssd->outputfile,"interleave read count: %13d\n",ssd->interleave_read_count);
	fprintf(ssd->outputfile,"interleave two plane and one program count: %13d\n",ssd->inter_mplane_prog_count);
	fprintf(ssd->outputfile,"interleave two plane count: %13d\n",ssd->inter_mplane_count);
	fprintf(ssd->outputfile,"gc copy back count: %13d\n",ssd->gc_copy_back);
	fprintf(ssd->outputfile,"write flash count: %13d\n",ssd->write_flash_count);
	fprintf(ssd->outputfile,"interleave erase count: %13d\n",ssd->interleave_erase_count);
	fprintf(ssd->outputfile,"multiple plane erase count: %13d\n",ssd->mplane_erase_conut);
	fprintf(ssd->outputfile,"interleave multiple plane erase count: %13d\n",ssd->interleave_mplane_erase_count);
	fprintf(ssd->outputfile,"read request count: %13d\n",ssd->read_request_count);
	fprintf(ssd->outputfile,"write request count: %13d\n",ssd->write_request_count);
	fprintf(ssd->outputfile,"read request average size: %13f\n",ssd->ave_read_size);
	fprintf(ssd->outputfile,"write request average size: %13f\n",ssd->ave_write_size);
	fprintf(ssd->outputfile,"read request average response time: %lld\n",read_avg_response_time);
	fprintf(ssd->outputfile,"write request average response time: %lld\n",write_avg_response_time);
	//fprintf(ssd->outputfile,"read request average response time: %lld\n",ssd->read_avg/ssd->read_request_count);
	//fprintf(ssd->outputfile,"write request average response time: %lld\n",ssd->write_avg/ssd->write_request_count);
	fprintf(ssd->outputfile,"buffer read hits: %13d\n",ssd->dram->buffer->read_hit);
	fprintf(ssd->outputfile,"buffer read miss: %13d\n",ssd->dram->buffer->read_miss_hit);
	fprintf(ssd->outputfile,"buffer write hits: %13d\n",ssd->dram->buffer->write_hit);
	fprintf(ssd->outputfile,"buffer write miss: %13d\n",ssd->dram->buffer->write_miss_hit);
	fprintf(ssd->outputfile,"erase: %13d\n",erase);
	fflush(ssd->outputfile);

	fclose(ssd->outputfile);


	fprintf(ssd->statisticfile,"\n");
	fprintf(ssd->statisticfile,"\n");
	fprintf(ssd->statisticfile,"---------------------------statistic data---------------------------\n");	
	fprintf(ssd->statisticfile,"min lsn: %13d\n",ssd->min_lsn);	
	fprintf(ssd->statisticfile,"max lsn: %13d\n",ssd->max_lsn);
	fprintf(ssd->statisticfile,"read count: %13d\n",ssd->read_count);	  
	fprintf(ssd->statisticfile,"program count: %13d",ssd->program_count);	  
	fprintf(ssd->statisticfile,"                        include the flash write count leaded by read requests\n");
	fprintf(ssd->statisticfile,"the read operation leaded by un-covered update count: %13d\n",ssd->update_read_count);
	fprintf(ssd->statisticfile,"erase count: %13d\n",ssd->erase_count);	  
	fprintf(ssd->statisticfile,"direct erase count: %13d\n",ssd->direct_erase_count);
	fprintf(ssd->statisticfile,"copy back count: %13d\n",ssd->copy_back_count);
	fprintf(ssd->statisticfile,"multi-plane program count: %13d\n",ssd->m_plane_prog_count);
	fprintf(ssd->statisticfile,"multi-plane read count: %13d\n",ssd->m_plane_read_count);
	fprintf(ssd->statisticfile,"interleave count: %13d\n",ssd->interleave_count);
	fprintf(ssd->statisticfile,"interleave read count: %13d\n",ssd->interleave_read_count);
	fprintf(ssd->statisticfile,"interleave two plane and one program count: %13d\n",ssd->inter_mplane_prog_count);
	fprintf(ssd->statisticfile,"interleave two plane count: %13d\n",ssd->inter_mplane_count);
	fprintf(ssd->statisticfile,"gc copy back count: %13d\n",ssd->gc_copy_back);
	fprintf(ssd->statisticfile,"write flash count: %13d\n",ssd->write_flash_count);
	fprintf(ssd->statisticfile,"waste page count: %13d\n",ssd->waste_page_count);
	fprintf(ssd->statisticfile,"interleave erase count: %13d\n",ssd->interleave_erase_count);
	fprintf(ssd->statisticfile,"multiple plane erase count: %13d\n",ssd->mplane_erase_conut);
	fprintf(ssd->statisticfile,"interleave multiple plane erase count: %13d\n",ssd->interleave_mplane_erase_count);
	fprintf(ssd->statisticfile,"read request count: %13d\n",ssd->read_request_count);
	fprintf(ssd->statisticfile,"write request count: %13d\n",ssd->write_request_count);
	fprintf(ssd->statisticfile,"read request average size: %13f\n",ssd->ave_read_size);
	fprintf(ssd->statisticfile,"write request average size: %13f\n",ssd->ave_write_size);
	fprintf(ssd->statisticfile,"read request average response time: %lld\n",read_avg_response_time);
	fprintf(ssd->statisticfile,"write request average response time: %lld\n",write_avg_response_time);
	//fprintf(ssd->statisticfile,"read request average response time: %lld\n",ssd->read_avg/ssd->read_request_count);
	//fprintf(ssd->statisticfile,"write request average response time: %lld\n",ssd->write_avg/ssd->write_request_count);
	fprintf(ssd->statisticfile,"buffer read hits: %13d\n",ssd->dram->buffer->read_hit);
	fprintf(ssd->statisticfile,"buffer read miss: %13d\n",ssd->dram->buffer->read_miss_hit);
	fprintf(ssd->statisticfile,"buffer write hits: %13d\n",ssd->dram->buffer->write_hit);
	fprintf(ssd->statisticfile,"buffer write miss: %13d\n",ssd->dram->buffer->write_miss_hit);
	fprintf(ssd->statisticfile,"erase: %13d\n",erase);
	fflush(ssd->statisticfile);

	fclose(ssd->statisticfile);
}


/***********************************************************************************
*根据每一页的状态计算出需要处理的子页的数目，也就是一个子请求需要处理的子页的页数
**@stored: 一个逻辑块中有效的页或一个页中有效的子页的状态，用如0x00100010的形式表示
			注意，int类型为4B，可表示32位
**@return: total, stored中值为1的位的个数
************************************************************************************/
unsigned int size(unsigned int stored)
{
	unsigned int i,total = 0,mask = 0x80000000;

	#ifdef DEBUG
		printf("enter size\n");
	#endif

	for(i = 1; i <= 32; i ++)
	{
		//mask中，只有1位为真
		if(stored & mask) 
			total ++;
		stored <<= 1;
	}
	#ifdef DEBUG
	    printf("leave size\n");
    #endif

    return total;
}

/*********************************************************
*transfer_size()函数的作用就是计算出子请求的需要处理的size
*函数中单独处理了first_lpn，last_lpn这两个特别情况，
因为这两种情况下很有可能不是处理一整页而是处理一页的一部分，
因为lsn有可能不是一页的第一个子页。
*********************************************************/
unsigned int transfer_size(struct ssd_info *ssd,
							int need_distribute,
							unsigned int lpn,
							struct request *req)
{
	unsigned int first_lpn,last_lpn,state,trans_size;
	unsigned int mask=0,offset1=0,offset2=0;

	first_lpn=req->lsn/ssd->parameter->subpage_page;
	last_lpn=(req->lsn+req->size-1)/ssd->parameter->subpage_page;

	mask=~(0xffffffff<<(ssd->parameter->subpage_page));
	state=mask;
	if(lpn==first_lpn)
	{
		offset1=ssd->parameter->subpage_page-((lpn+1)*ssd->parameter->subpage_page-req->lsn);
		state=state&(0xffffffff<<offset1);
	}
	if(lpn==last_lpn)
	{
		offset2=ssd->parameter->subpage_page-((lpn+1)*ssd->parameter->subpage_page-(req->lsn+req->size));
		state=state&(~(0xffffffff<<offset2));
	}

	trans_size=size(state&need_distribute);

	return trans_size;
}


/*****************************************************************************************  
find_nearest_event 寻找"所有"子请求下个状态的最早到达的时间

循环遍历所有channel, 及每个channel中的所有chip, 
如果channel, chip上(有请求)正在被占用, 就不用麻烦了, 返回MAX_INT64;
如果channel, chip的next_state为idle(比如当前正在进行数据传输, 传输完成之后为idle, 
但是要保证这个时间比current_time大, 因为至少要表示这里有子请求正在处理), 取得这个idle状态到来的时间.
比较所有channel, chip的这个时间, 返回其中最小的, 因为要处理其中最近的.
*****************************************************************************************/
int64_t find_nearest_event(struct ssd_info *ssd) 
{
	unsigned int i, j;
	//int64_t是有符号64位整型，表示介于-2^63到2^63-1之间的整数, 8字节。
	int64_t time = MAX_INT64;
	int64_t time1 = MAX_INT64;
	int64_t time2 = MAX_INT64;

	for (i = 0; i < ssd->parameter->channel_number; i ++)
	{
		if (ssd->channel_head[i].next_state == CHANNEL_IDLE)
			if(time1 > ssd->channel_head[i].next_state_predict_time)
				if (ssd->channel_head[i].next_state_predict_time > ssd->current_time)    
					time1 = ssd->channel_head[i].next_state_predict_time;
		for (j = 0; j < ssd->parameter->chip_channel[i]; j ++)
		{
			if ((ssd->channel_head[i].chip_head[j].next_state==CHIP_IDLE) ||
				(ssd->channel_head[i].chip_head[j].next_state==CHIP_DATA_TRANSFER))
				if(time2 > ssd->channel_head[i].chip_head[j].next_state_predict_time)
					if (ssd->channel_head[i].chip_head[j].next_state_predict_time > ssd->current_time)    
						time2 = ssd->channel_head[i].chip_head[j].next_state_predict_time;
		}
	}
	time = (time1 > time2) ? time2 : time1;
	return time;
}

/***********************************************
*free_all_node()函数的作用就是释放所有申请的节点
************************************************/
void free_all_node(struct ssd_info *ssd)
{
	unsigned int i,j,k,l,n;
	struct buffer_group *pt=NULL;
	struct direct_erase *erase_node=NULL;
	for (i=0;i<ssd->parameter->channel_number;i++)
	{
		for (j=0;j<ssd->parameter->chip_channel[0];j++)
		{
			for (k=0;k<ssd->parameter->die_chip;k++)
			{
				for (l=0;l<ssd->parameter->plane_die;l++)
				{
					for (n=0;n<ssd->parameter->block_plane;n++)
					{
						free(ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].blk_head[n].page_head);
						ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].blk_head[n].page_head=NULL;
					}
					free(ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].blk_head);
					ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].blk_head=NULL;
					while(ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].erase_node!=NULL)
					{
						erase_node=ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].erase_node;
						ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].erase_node=erase_node->next_node;
						free(erase_node);
						erase_node=NULL;
					}
				}
				free(ssd->channel_head[i].chip_head[j].die_head[k].plane_head);
				ssd->channel_head[i].chip_head[j].die_head[k].plane_head=NULL;
			}
			free(ssd->channel_head[i].chip_head[j].die_head);
			ssd->channel_head[i].chip_head[j].die_head=NULL;
		}
		free(ssd->channel_head[i].chip_head);
		ssd->channel_head[i].chip_head=NULL;
	}
	free(ssd->channel_head);
	ssd->channel_head=NULL;

	avlTreeDestroy( ssd->dram->buffer);
	ssd->dram->buffer=NULL;
	
	free(ssd->dram->map->map_entry);
	ssd->dram->map->map_entry=NULL;
	free(ssd->dram->map);
	ssd->dram->map=NULL;
	free(ssd->dram);
	ssd->dram=NULL;
	free(ssd->parameter);
	ssd->parameter=NULL;

	free(ssd);
	ssd=NULL;
}


/*****************************************************************************
*make_aged()函数的作用就是模拟真实的用过一段时间的ssd，
*那么这个ssd的相应的参数就要改变，所以这个函数实质上就是对ssd中各个参数的赋值。
******************************************************************************/
struct ssd_info *make_aged(struct ssd_info *ssd)
{
	unsigned int i,j,k,l,m,n,ppn;
	int threshould,flag=0;
    
	if (ssd->parameter->aged == 1)
	{
		//threshold表示一个plane中有多少页需要提前置为失效
		threshould=(int)(ssd->parameter->block_plane * 
						 ssd->parameter->page_block *
						 ssd->parameter->aged_ratio); 
		for (i=0;i<ssd->parameter->channel_number;i++)
			for (j=0;j<ssd->parameter->chip_channel[i];j++)
				for (k=0;k<ssd->parameter->die_chip;k++)
					for (l=0;l<ssd->parameter->plane_die;l++)
					{  
						flag = 0;
						for (m=0;m<ssd->parameter->block_plane;m++)
						{  
							if (flag >= threshould)
							{
								break;
							}
							for (n = 0; n < (ssd->parameter->page_block * ssd->parameter->aged_ratio + 1); n ++)
							{
								//表示某一页失效，同时标记valid和free状态都为0
								ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].blk_head[m].page_head[n].valid_state=0;        
								ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].blk_head[m].page_head[n].free_state=0;         
								
								//把valid_state free_state lpn都置为0表示页失效，检测的时候三项都检测，单独lpn=0可以是有效页
								ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].blk_head[m].page_head[n].lpn=0;

								ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].blk_head[m].free_page_num--;
								ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].blk_head[m].invalid_page_num++;
								ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].blk_head[m].last_write_page++;
								ssd->channel_head[i].chip_head[j].die_head[k].plane_head[l].free_page--;
								
								flag++;
								//这句话并没有什么用嘛
								ppn=find_ppn(ssd,i,j,k,l,m,n);
							}
						} 
					}	 
	}  
	else
	{
		return ssd;
	}

	return ssd;
}


/*********************************************************************************************
*no_buffer_distribute()函数是处理当ssd没有dram的时候，
*这是读写请求就不必再需要在buffer里面寻找，
*直接利用creat_sub_request()函数创建子请求，再处理。
*********************************************************************************************/
struct ssd_info *no_buffer_distribute(struct ssd_info *ssd)
{
	unsigned int lsn,lpn,last_lpn,first_lpn,complete_flag=0, state;
	unsigned int flag = 0,flag1 = 1,active_region_flag = 0;           //to indicate the lsn is hitted or not
	struct request *req=NULL;
	struct sub_request *sub=NULL,*sub_r=NULL,*update=NULL;
	struct local *loc=NULL;
	struct channel_info *p_ch=NULL;

	
	unsigned int mask=0; 
	unsigned int offset1=0, offset2=0;
	unsigned int sub_size=0;
	unsigned int sub_state=0;

	ssd->dram->current_time = ssd->current_time;
	req = ssd->request_tail;
	lsn = req->lsn;
	first_lpn = req->lsn/ssd->parameter->subpage_page;
	lpn = req->lsn/ssd->parameter->subpage_page;
	last_lpn = (req->lsn+req->size-1)/ssd->parameter->subpage_page;

	if(req->operation == READ)        
	{		
		while(lpn <= last_lpn)
		{
			sub_state = (ssd->dram->map->map_entry[lpn].state & 0x7fffffff);
			sub_size = size(sub_state);
			sub = creat_sub_request(ssd,lpn,sub_size,sub_state,req,req->operation);
			lpn ++;
		}
	}
	else if(req->operation == WRITE)
	{
		while(lpn<=last_lpn)     	
		{	
			mask=~(0xffffffff<<(ssd->parameter->subpage_page));
			state=mask;
			if(lpn==first_lpn)
			{
				offset1=ssd->parameter->subpage_page-((lpn+1)*ssd->parameter->subpage_page-req->lsn);
				state=state&(0xffffffff<<offset1);
			}
			if(lpn==last_lpn)
			{
				offset2=ssd->parameter->subpage_page-((lpn+1)*ssd->parameter->subpage_page-(req->lsn+req->size));
				state=state&(~(0xffffffff<<offset2));
			}
			sub_size=size(state);

			sub=creat_sub_request(ssd,lpn,sub_size,state,req,req->operation);
			lpn++;
		}
	}

	return ssd;
}
