# ssdsim linux support
all:ssd 
	
clean:
	rm -f ssd *.o *~
.PHONY: clean

ssd: ssd.o avlTree.o flash.o initialize.o pagemap.o     
	cc -g3 -o ssd ssd.o avlTree.o flash.o initialize.o pagemap.o
ssd.o: flash.h initialize.h pagemap.h
	gcc -c -g3 ssd.c
flash.o: pagemap.h
	gcc -c -g3 flash.c
initialize.o: avlTree.h pagemap.h
	gcc -c -g3 initialize.c
pagemap.o: initialize.h
	gcc -c -g3 pagemap.c
avlTree.o: 
	gcc -c -g3 avlTree.c

